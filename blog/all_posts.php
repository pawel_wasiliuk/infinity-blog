<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/blog/view/header.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/blog/view/search.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/blog/helpers/all_posts.php");

if (isset($_POST['search'])) {
    //handle the empty query case
    if (empty($_POST['search'])) {
        echo('<script>$(".content").empty();</script>');
        require_once($_SERVER['DOCUMENT_ROOT'] . "/blog/view/footer.php");
        return;
    }
    //handle search query
    $post = new posts();
    $post->setSearchPostQtyToZero();
    echo('<script>$(".content").empty();</script>');
    $searchedPosts = $post->getAllPostsQty();
    $postsFound = 0;

    for ($i = 1; $i <= $searchedPosts; $i++) {
        $record = $post->findPostByDbPlace($i);
        $result = strstr($record[5], $_POST['search']);
        if ($result == false || $postsFound == 10) {
            continue;
        } else {
            $updatedExcerpt = "";
            $position = strpos($record[5], $_POST['search']);
            $bodyLength = strlen($record[5]) - 1;
            if ($position > 20) {
                $startIndex = $position - 20;
                $updatedExcerpt = "..." . substr($record[5], $startIndex, 20) . '<a style="background-color:#ffff00;">'
                    . $_POST['search'] . '</a>' . substr($record[5], $position + strlen($_POST['search']), 160) . "...";
                $post->transmitNewExcerptToDb($record[0], $updatedExcerpt);
            } else {
                $startIndex = 0;
                $updatedExcerpt = "..." . '<a style="background-color:#ffff00;">' . $_POST['search'] . '</a>' .
                    substr($record[5], $position + strlen($_POST['search']), 180) . "...";
                $post->transmitNewExcerptToDb($record[0], $updatedExcerpt);
            }
            //found article by search
            $elementToAdd = '<article class="topContent"><header><h2><a href="#" title="' . $record[1] . '">' .
                $record[1] . '</a></h2></header><footer><p class="post-author">Author: ' . $record[2] . '</p>' .
                '</footer><content>' . $updatedExcerpt . '</content><footer><p class="post-date">Publish date: ' .
                $record[3] . '</p>' . '<p class="post-time">Publish time: ' . $record[4] . '</p><form><button 
                    type="submit" title="Delete post"' . ' value="' . $record[0] . '" class="del_btn">Delete post</button>
                    <button type="submit" title="Edit post"' . ' value="' . $record[0] . '" class="edit_btn">Edit post
                    </button><button type="submit" title="Open post in a new tab" value="' . $record[0] . '" 
                    class="highlighted_content_btn">Open Post</button></form></footer></article>';
            ?>
            <script>
                var elementToAdd = <?php echo json_encode($elementToAdd); ?>;
                $(".content").append(elementToAdd);
            </script>
            <?php
            $postsFound++;
        }
    }
    $post->updateSearchPostQty($postsFound);
    $post->updateSearchQuery($_POST['search']);
    //display search button
    require_once($_SERVER['DOCUMENT_ROOT'] . "/blog/view/show_search_posts.php");
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/blog/view/show_more_posts.php");
if (isset($_POST['search'])) {
    echo('<script>$(".more_posts_btn").hide();</script>');
}
require_once($_SERVER['DOCUMENT_ROOT'] . "/blog/view/footer.php");