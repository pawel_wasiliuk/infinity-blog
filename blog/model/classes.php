<?php

class posts
{
    private $title, $author, $body;
    private $displayedPostsCounter = 0;

    public function displayError($errorString)
    {
        echo '<script type="text/javascript">alert("' . $errorString . '"); </script>';
    }

    public function getDisplayedPostsCounter()
    {
        return $this->displayedPostsCounter;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function addPost()
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        date_default_timezone_set('Europe/Riga');
        $date = date('Y-m-d');
        $time = strftime('%H %M %S');
        $excerpt = $this->getExcerpt($this->getBody(), $startPos = 0, $maxLength = 200);

        $query = $db->prepare("INSERT INTO posts (title, author, date, time, body, excerpt)
                              VALUES (:title, :author, :date, :time, :body, :excerpt)");
        $query->execute(array(
            'title' => $this->getTitle(),
            'author' => $this->getAuthor(),
            'date' => $date,
            'time' => $time,
            'body' => $this->getBody(),
            'excerpt' => $excerpt
        ));
        $this->displayResult("Post was successfully created!");
    }

    function getExcerpt($str, $startPos = 0, $maxLength = 100)
    {
        if (strlen($str) > $maxLength) {
            $excerpt = substr($str, $startPos, $maxLength - 3);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt = substr($excerpt, 0, $lastSpace);
            $excerpt .= '...';
        } else {
            $excerpt = $str;
        }
        return $excerpt;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function displayResult($resultString)
    {
        echo '<script type="text/javascript">alert("' . $resultString . '"); </script>';
    }

    public function updatePost($postIndex)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        date_default_timezone_set('Europe/Riga');
        $date = date('Y-m-d');
        $time = strftime('%H %M %S');
        $excerpt = $this->getExcerpt($this->getBody(), $startPos = 0, $maxLength = 200);

        $query = $db->prepare("UPDATE posts SET title = :title, author = :author, date = :date, time = :time, body = :body,
                              excerpt = :excerpt WHERE post_id = :post_id");
        $query->execute(array(
            'title' => $this->getTitle(),
            'author' => $this->getAuthor(),
            'date' => $date,
            'time' => $time,
            'body' => $this->getBody(),
            'excerpt' => $excerpt,
            'post_id' => $postIndex
        ));
        $this->displayResult("Post was successfully edited!");
        echo '<script type="text/javascript">window.location = "/blog/all_posts.php"</script>';
    }

    //function for displaying the very first 10 or less posts when page loads
    public function getPostsQty($postsDisplayed)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM posts");
        $query->execute();
        $loopCounter = 0;
        $postsQty = 0;
        while ($postData = $query->fetch()) {
            $loopCounter++;
            //iterate along if already displayed post
            if ($loopCounter <= $postsDisplayed) {
                continue;
            }
            if ($postsQty == 10) {
                return $postsQty;
            }
            $postsQty++;
        }
        return $postsQty;
    }

    //get unique post according its position in db
    public function getPost($foundQty)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM posts");
        $query->execute();
        $foundRecordsInScope = 0;

        while ($postData = $query->fetch()) {
            $foundRecordsInScope++;
            if ($foundRecordsInScope <= $foundQty) {
                continue;
            }
            return $postData;
        }
    }

    public function transmitPostsQtyToDb()
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM posts_qty");
        $query->execute();

        //if record exists - truncate table
        while ($query->fetch()) {
            $truncateQuery = $db->prepare("TRUNCATE posts_qty");
            $truncateQuery->execute();
        }

        $insertQuery = $db->prepare("INSERT INTO posts_qty () VALUES ()");
        $insertQuery->execute();
    }

    public function updatePostsQtyInDb($value)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM posts_qty");
        $query->execute();
        $previousValue = 0;
        while ($foundRecord = $query->fetch()) {
            $previousValue = (int)$foundRecord[0];
        }
        $updatedValue = (int)$value + $previousValue;
        $updateQuery = $db->prepare("UPDATE posts_qty SET posts_qty = :posts_qty");
        $updateQuery->execute(array(
            'posts_qty' => $updatedValue
        ));
    }

    //get the number of posts stored as 'posts_qty' table items
    public function getPostsQtyFromDb()
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM posts_qty");
        $query->execute();
        while ($record = $query->fetch()) {
            return $record[0];
        }
    }

    //get the number of all posts that were added to 'posts' table
    public function getAllPostsQty()
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM posts");
        $query->execute();
        $count = $query->rowCount();
        return $count;
    }

    //delete post by id value (auto-generated index)
    public function deletePostByIndex($index)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("DELETE FROM posts WHERE post_id = :post_id");
        $query->execute(array(
            'post_id' => $index
        ));
    }

    //fill table 'editing_post' with the data of a post opened in edit mode
    public function fillEditingPostData($index)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        //check whether there is any post in this temp table
        $recordExists = false;
        $check_query = $db->prepare("SELECT * FROM editing_post");
        $check_query->execute();

        while ($checkData = $check_query->fetch()) {
            $recordExists = true;
            break;
        }

        if ($recordExists) {
            $truncate_query = $db->prepare("TRUNCATE editing_post");
            $truncate_query->execute();
        }

        $query = $db->prepare("SELECT * FROM posts WHERE post_id = :post_id");
        $query->execute(array(
            'post_id' => $index
        ));

        //retrieve data from posts table
        while ($postData = $query->fetch()) {

            $query = $db->prepare("INSERT INTO editing_post (post_id, title, author, date, time, body, excerpt)
                              VALUES (:post_id, :title, :author, :date, :time, :body, :excerpt)");
            $query->execute(array(
                'post_id' => $index,
                'title' => $postData[1],
                'author' => $postData[2],
                'date' => $postData[3],
                'time' => $postData[4],
                'body' => $postData[5],
                'excerpt' => $postData[6]
            ));
        }
    }

    //data of a post opened in edit mode (to be overwritten or not)
    public function getOldPostData()
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM editing_post");
        $query->execute();

        while ($postData = $query->fetch())
            return $postData;
    }

    public function deleteAllPosts()
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("TRUNCATE posts");
        $query->execute();
    }

    //db place means row post is located at
    public function findPostByDbPlace($postDbNumber)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM posts");
        $query->execute();
        $loopCounter = 0;
        while ($postData = $query->fetch()) {
            $loopCounter++;
            if ($loopCounter < $postDbNumber) {
                continue;
            } else if ($loopCounter == $postDbNumber) {
                return $postData;
            }
        }
    }

    public function truncateExcerptTableOnReload()
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("TRUNCATE excerpt");
        $query->execute();
    }

    public function transmitNewExcerptToDb($recordId, $excerpt)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("INSERT INTO excerpt (post_id, updated_excerpt)
                              VALUES (:post_id, :updated_excerpt)");
        $query->execute(array(
            'post_id' => $recordId,
            'updated_excerpt' => $excerpt
        ));
    }

    //display new post excerpt as a successful search query result
    public function getExcerptByPostId($postId)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT updated_excerpt FROM posts WHERE post_id = :post_id");
        $query->execute(array(
            'post_id' => $postId
        ));
        while ($postData = $query->fetch()) {
            return $postData[1]; //returning new excerpt
        }
    }

    //when page reloads, search starts from 0
    public function setSearchPostQtyToZero()
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $updateQuery = $db->prepare("UPDATE search_posts_qty SET search_posts_qty = 0");
        $updateQuery->execute();
    }

    public function updateSearchPostQty($postsDisplayedInSearch)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM search_posts_qty");
        $query->execute();

        while ($foundRecords = $query->fetch()) {
            $previousValue = (int)$foundRecords[0];
            $postsDisplayedInSearch += $previousValue;
        }

        $updateQuery = $db->prepare("UPDATE search_posts_qty SET search_posts_qty = :search_posts_qty");
        $updateQuery->execute(array(
            'search_posts_qty' => $postsDisplayedInSearch
        ));
    }

    public function getSearchPostQty()
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM search_posts_qty");
        $query->execute();
        while ($foundRecords = $query->fetch()) {
            return $foundRecords[0];
        }
    }

    public function updateSearchQuery($searchQuery)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $updateQuery = $db->prepare("UPDATE search_query SET search_query = :search_query");
        $updateQuery->execute(array(
            'search_query' => $searchQuery
        ));
    }

    public function getSearchQuery()
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        $query = $db->prepare("SELECT * FROM search_query");
        $query->execute();

        while ($searchData = $query->fetch()) {
            return $searchData[0];
        }
    }

    //update post's content by highlighting its search query phrase(in open mode).
    public function fillSearchPostData($index, $searchQuery)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        //check whether there is any post in this temp table
        $recordExists = false;
        $check_query = $db->prepare("SELECT * FROM editing_post");
        $check_query->execute();

        while ($checkData = $check_query->fetch()) {
            $recordExists = true;
            break;
        }

        if ($recordExists) {
            $truncate_query = $db->prepare("TRUNCATE editing_post");
            $truncate_query->execute();
        }

        $query = $db->prepare("SELECT * FROM posts WHERE post_id = :post_id");
        $query->execute(array(
            'post_id' => $index
        ));

        //retrieve data from posts table
        while ($postData = $query->fetch()) {
            $array = $this->strposAll($postData[5], $searchQuery);
            for ($x = 0; $x < sizeof($array); $x++) {
                echo $array[$x];
            }
            $arrayCounter = 0;
            $updateString = "";
            for ($i = 0; $i < strlen($postData[5]); $i++) {
                if ($arrayCounter < sizeof($array) && $array[$arrayCounter] == $i) {
                    $updateString .= '<a style="background-color:#ffff00;">';
                    for ($j = 0; $j < strlen($searchQuery); $j++) {
                        $updateString .= $searchQuery[$j];
                        if ($j != strlen($searchQuery) - 1)
                            $i++;
                    }
                    $updateString .= '</a>';
                    $arrayCounter++;
                } else {
                    $updateString .= $postData[5][$i];
                }
            }

            $query = $db->prepare("INSERT INTO editing_post (post_id, title, author, date, time, body, excerpt)
                              VALUES (:post_id, :title, :author, :date, :time, :body, :excerpt)");
            $query->execute(array(
                'post_id' => $index,
                'title' => $postData[1],
                'author' => $postData[2],
                'date' => $postData[3],
                'time' => $postData[4],
                'body' => $updateString,
                'excerpt' => $postData[6]
            ));
        }
    }

    //function that returns an array of all needle matches in haystack
    public function strposAll($haystack, $needle)
    {
        $offset = 0;
        $allpos = array();
        while (($pos = strpos($haystack, $needle, $offset)) !== FALSE) {
            $offset = $pos + 1;
            $allpos[] = $pos;
        }
        return $allpos;
    }

    public function generateNewPosts($postsQty, $titleData, $personData, $contentData)
    {
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/connection.php");
        for ($i = 0; $i < $postsQty; $i++) {
            $title = $titleData[array_rand($titleData)];
            $person = $personData[array_rand($personData)];
            $content = "";
            for ($j = 0; $j < 3; $j++) {
                $content .= $contentData[array_rand($contentData)];
            }
            date_default_timezone_set('Europe/Riga');
            $date = date('Y-m-d');
            $time = strftime('%H %M %S');
            $excerpt = $this->getExcerpt($content, $startPos = 0, $maxLength = 200);

            $query = $db->prepare("INSERT INTO posts (title, author, date, time, body, excerpt)
                              VALUES (:title, :author, :date, :time, :body, :excerpt)");
            $query->execute(array(
                'title' => $title,
                'author' => $person,
                'date' => $date,
                'time' => $time,
                'body' => $content,
                'excerpt' => $excerpt
            ));
        }
    }
}