<?php
if (isset($_POST['search'])) {
    include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/classes.php");
    $post = new posts();
    $searchedPosts = $post->getAllPostsQty();
    $allPostsMatches = 0;
    $postsFound = 0;
    $displayedSearchPostsQty = $post->getSearchPostQty();

    for ($i = 1; $i <= $searchedPosts; $i++) {
        $record = $post->findPostByDbPlace($i);
        $result = strstr($record[5], $_POST['search']);
        if ($result == false || $postsFound == 10) {
            continue;
        } else if ($allPostsMatches <= $displayedSearchPostsQty) { //posts that've been already found & displayed
            $allPostsMatches++;
            continue;
        } else {
            $updatedExcerpt = "";
            $position = strpos($record[5], $_POST['search']);
            $bodyLength = strlen($record[5]) - 1;
            if ($position > 20) {
                $startIndex = $position - 20;
                $updatedExcerpt = "..." . substr($record[5], $startIndex, 20) . '<a style="background-color:#ffff00;">'
                    . $_POST['search'] . '</a>' . substr($record[5], $position + strlen($_POST['search']), 160) . "...";
                $post->transmitNewExcerptToDb($record[0], $updatedExcerpt);
            } else {
                $startIndex = 0;
                $updatedExcerpt = "..." . '<a style="background-color:#ffff00;">' . $_POST['search'] . '</a>' .
                    substr($record[5], $position + strlen($_POST['search']), 180) . "...";
                $post->transmitNewExcerptToDb($record[0], $updatedExcerpt);
            }
            //found article by search
            $elementToAdd = '<article class="topContent"><header><h2><a href="#" title="' . $record[1] . '">' .
                $record[1] . '</a></h2></header><footer><p class="post-author">Author: ' . $record[2] . '</p>' .
                '</footer><content>' . $updatedExcerpt . '</content><footer><p class="post-date">Publish date: ' .
                $record[3] . '</p>' . '<p class="post-time">Publish time: ' . $record[4] . '</p><form><button 
                    type="submit" title="Delete post"' . ' value="' . $record[0] . '" class="del_btn">Delete post</button>
                    <button type="submit" title="Edit post"' . ' value="' . $record[0] . '" class="edit_btn">Edit post
                    </button><button type="submit" title="Open post in a new tab" value="' . $record[0] . '" 
                    class="highlighted_content_btn">Open Post</button></form></footer></article>';
            echo($elementToAdd);
            $postsFound++;
        }
    }
    $post->updateSearchPostQty($postsFound);
}