<div class="content">
    <article class="topContent">
        <header>
            <h2><a class="addPostReference" href="#" title="Edit existing post">Edit existing post</a></h2>
        </header>
        <?php
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/classes.php");
        $post = new posts();
        $oldPost = $post->getOldPostData();
        //set post's values to the form's fields
        ?>
        <footer>
            <form id="newPostForm" action="/blog/helpers/updatePost.php" method="post">
                <label id="labelTitle">Title:</label>
                <input id="titleInput" name="title" type="text" value="<?php echo($oldPost[6]);?>">
                <label id="labelAuthor">Author:</label>
                <input id="authorInput" name="author" type="text" value="<?php echo($oldPost[1])?>">
                <label id="labelContent">Content:</label>
                <textarea id="bodyInput" name="body"><?php echo($oldPost[2]);?></textarea>
                <button id="editPost" type="submit">Edit existing post</button>
            </form>
        </footer>
        </article>
</div>