<?php
if (isset($_POST['action'])) {
    include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/classes.php");
    $post = new posts();
    $postsToDisplay = $post->getPostsQty($post->getPostsQtyFromDb());
    $newRecordsIndex = $post->getPostsQtyFromDb();
    for ($i = 1; $i <= $postsToDisplay; $i++) {
        $record = $post->getPost($newRecordsIndex);
        $newRecordsIndex++;
        $elementToAdd = '<article class="topContent"><header><h2><a href="#" title="' . $record[1] . '">' .
            $record[1] . '</a></h2></header><footer><p class="post-author">Author: ' . $record[2] . '</p>' .
            '</footer><content>' . $record[6] . '</content><footer><p class="post-date">Publish date: ' .
            $record[3] . '</p>' . '<p class="post-time">Publish time: ' . $record[4] . '</p><form><button type="submit" 
            title="Delete post"' . ' value="' . $record[0] . '" class="del_btn">Delete post</button><button type="submit" 
            title="Edit post"' . ' value="' . $record[0] . '" class="edit_btn">Edit post</button><button type="submit" 
            title="Open post in a new tab" value="' . $record[0] . '" class="post_content_btn">Open Post</button>
            </form></footer></article>';
        echo($elementToAdd);
    }
    $post->updatePostsQtyInDb($postsToDisplay);
}