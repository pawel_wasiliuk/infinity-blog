<div class="mainContent">
    <div class="content">
        <?php
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/helpers/show_more_posts.php");
        include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/classes.php");

        $post = new posts();
        $post->truncateExcerptTableOnReload();
        $post->transmitPostsQtyToDb();
        $postsDisplayed = 0;
        $postsQty = $post->getPostsQty($postsDisplayed);
        for ($i = 1; $i <= $postsQty; $i++) {
            $record = $post->getPost($postsDisplayed);
            $postsDisplayed++;
            ?>
            <article class="topContent">
                <header>
                    <h2><a href="#" title="<?php echo($record[1]); ?>"><?php echo($record[1]); ?></a></h2>
                </header>

                <footer>
                    <p class="post-author">Author: <?php echo($record[2]); ?></p>
                </footer>

                <content>
                    <?php echo($record[6]); ?>
                </content>

                <footer>
                    <p class="post-date">Publish date: <?php echo($record[3]); ?></p>
                    <p class="post-time">Publish time: <?php echo($record[4]); ?></p>
                    <form>
                        <button type="submit" title="Delete post" value="<?php echo($record[0]); ?>" class="del_btn">
                            Delete post
                        </button>
                        <button type="submit" title="Edit post" value="<?php echo($record[0]); ?>" class="edit_btn">Edit
                            post
                        </button>
                        <button type="submit" title="Open post in a new tab" value="<?php echo($record[0]); ?>"
                                class="post_content_btn">Open Post
                        </button>
                        <button type="submit" title="Open post in a new tab" value="<?php echo($record[0]); ?>"
                                class="highlighted_content_btn" style="display:none;">Open Post
                        </button>
                    </form>
                </footer>
            </article>
            <?php
        } ?>
    </div>
</div>