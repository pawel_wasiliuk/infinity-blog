<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/blog/view/header.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/blog/helpers/edit_post.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/blog/view/footer.php");

if (isset($_POST['title']) && isset($_POST['author']) && isset($_POST['body'])) {

    if (empty($_POST['title'])) {
        $post->displayError("You must provide your post title!");

    } else if (empty($_POST['author'])) {
        $post->displayError("You must provide your author's name!");

    } else if (empty($_POST['body'])) {
        $post->displayError("Content of the post must be filled!");

    } else {
        $post->setTitle($_POST['title']);
        $post->setAuthor($_POST['author']);
        $post->setBody($_POST['body']);
        $post->updatePost($oldPost[0]);
        header("Refresh:0");
    }
}