<?php
if (isset($_POST['checked']) && isset($_POST['postsNumber'])) {
    if ($_POST['postsNumber'] <= 0) {
        return;
    }

    include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/classes.php");
    $post = new posts();

    //fields for random post title generating
    $titleData = array("How I Improved My Random In One Easy Lesson", "What Can You Do About Random Right Now",
        "The Secrets To Random", "How To Handle Every Random Challenge With Ease Using These Tips",
        "The Untold Secret To Random In Less Than Ten Minutes", "The Hidden Mystery Behind Random",
        "Stop Wasting Time And Start Random", "Never Suffer From Random Again", "Beware The Random Scam",
        "Random Smackdown!");
    //fields for random person data generating
    $personData = array("Tamara Roberts", "Kelley Warren", "Wayne Gonzales", "Guadalupe Harper", "Dorothy Walsh",
        "Julie Garner", "Rex Rose", "Kirk Yates", "Rhonda Waters", "Faye Barker");
    //fields for random post content generating (3 random fields per post).
    $contentData = array("Birkendose getunchten em vertreiben zu ei da.", "Geblieben senkrecht zu ja spurenden unterwegs",
        "Achthausen wie scherzwort ten ein gesprachig alt bodenlosen.", "Unruhig bruchig brummte du zu er wachter.",
        "Alt verschwand vorpfeifen ein marktplatz geheiratet weg befangenen bescheiden.",
        "Kammer wandte bandes jedoch schale zu du.", "Dazwischen regungslos des das birkendose nettigkeit nachmittag oha.",
        "Braten fragen ers war kinder ins.", "Zitterte vor brauerei gelandes tut neustadt.",
        "Als nur betrubte zog vorstadt gewartet sag.", "Liebevoll dus behaglich arbeitete sah kindliche tun kellnerin kammertur.",
        "Gesteckt frohlich ins ist hut trillern vollends launisch.", "Eleonora gespielt gegessen die geholfen geworden oha.",
        "Da in hochstens schnellen duftenden unendlich teilnahme wohnstube.", "Tadtchen da ja zu bewirtung dahinging.");
    if ($_POST['checked'] == "false") {
        $post->deleteAllPosts();
    }
    $post->generateNewPosts($_POST['postsNumber'], $titleData, $personData, $contentData);
}