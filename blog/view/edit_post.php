<div class="content">
        <article class="topContent">
        <header>
            <h2><a class="addPostReference" href="#" title="Add new post">Edit existing post</a></h2>
        </header>

        <footer>
            <form id="newPostForm" action="/blog/create_post.php" method="post">
            <label id="labelTitle">Title:</label>
            <input id="titleInput" name="title" type="text" value="">
            <label id="labelAuthor">Author:</label>
            <input id="authorInput" name="author" type="text" value="">
            <label id="labelContent">Content:</label>
            <textarea id="bodyInput" name="body"></textarea>
            <button id="create_post_btn" type="submit">Edit post</button>
            </form>
        </footer>
        </article>
</div>