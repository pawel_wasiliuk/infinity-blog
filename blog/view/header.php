<!DOCTYPE html>
<html>
<head>
    <title>Infinite Software Blog</title>
    <meta name="viewport" content="width=device-width, scale=1.0">
    <script type="text/javascript" src="/blog/jquery/jquery-3.2.0.min.js"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.del_btn', function () {
                var clickBtnValue = $(this).val();
                var ajaxurl = '/blog/helpers/delete_post.php',
                    data = {'action': clickBtnValue};
                $.post(ajaxurl, data, function () {
                    alert("Post was successfully deleted");
                });
            });
            $(document).on('click', '.edit_btn', function () {
                var clickBtnValue = $(this).val();
                var ajaxurl = '/blog/edit_post.php',
                    data = {'action': clickBtnValue};
                $.post(ajaxurl, data, function () {
                    window.location.assign("/blog/helpers/updatePost.php");
                });
            });
            $(document).on('click', '.post_content_btn', function () {
                var clickBtnValue = $(this).val();
                var ajaxurl = '/blog/view/post.php',
                    data = {'action': clickBtnValue};
                $.post(ajaxurl, data, function () {
                    window.location.assign("/blog/post.php");
                });
            });
            $(document).on('click', '.highlighted_content_btn', function () {
                var clickBtnValue = $(this).val();
                var ajaxurl = '/blog/view/highlighted_post.php',
                    data = {'action': clickBtnValue};
                $.post(ajaxurl, data, function () {
                    window.location.assign("/blog/highlighted_post.php");
                });
            });
            $('.more_posts_btn').click(function () {
                var clickBtnValue = $(this).val();
                var ajaxurl = '/blog/helpers/show_more_posts.php',
                    data = {'action': clickBtnValue};
                $.post(ajaxurl, data, function (output) {
                    $(".content").append(output);
                });
            });
            $('.search_posts').click(function () {
                var clickBtnValue = $(this).val();
                var ajaxurl = '/blog/helpers/show_search_posts.php',
                    data = {'search': clickBtnValue};
                $.post(ajaxurl, data, function (output) {
                    $(".content").append(output);
                });
            });
            $('#delete_posts_btn').click(function () {
                var clickBtnValue = $(this).val();
                var ajaxurl = '/blog/helpers/delete_all_posts.php',
                    data = {'action': clickBtnValue};
                $.post(ajaxurl, data, function () {
                });
            });
            $('#generate_posts_btn').click(function () {
                var checkValue;
                var postsNumber = document.getElementById("generateNumber").value;
                if (document.getElementById('appendCheckbox').checked) {
                    checkValue = true;
                }
                else {
                    checkValue = false;
                }
                var ajaxurl = '/blog/helpers/generate_posts.php',
                    data = {'checked': checkValue, 'postsNumber': postsNumber};
                $.post(ajaxurl, data, function () {
                    alert("Posts were successfully generated!");
                });
            });
        });
    </script>
    <link rel="stylesheet" href="/blog/styles/style.css" type="text/css"/>
</head>

<body class="body">
<header class="mainHeader">
    <nav>
        <ul>
            <li><a href="/blog/index.php">Home</a></li>
            <li><a href="/blog/create_post.php">Create Post</a></li>
            <li><a href="/blog/all_posts.php">All Posts</a></li>
            <li><a href="/blog/delete_and_generate.php">Delete posts / Generate posts</a></li>
        </ul>
    </nav>
</header>