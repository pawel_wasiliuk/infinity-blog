<div class="content">
    <article class="topContent">
        <footer>
            <form action="/blog/delete_and_generate.php" method="post">
                <button id="delete_posts_btn" type="submit">Delete all posts</button>
            </form>
            <p id="sectionDescription">Permanently delete every post in the blog</p>
            <br>
            <form action="/blog/delete_and_generate.php" method="post">
                <button id="generate_posts_btn" type="submit">Generate new posts</button>
                </br>
                <input type="number" id="generateNumber">
                <span id="sectionDescription">Type number of posts you want to generate</span><br/>
                <input type="checkbox" id="appendCheckbox" name="appendCheckbox" value=""><label
                    for="appendCheckbox"><span>Append</span></label>
            </form>
            <p id="sectionDescription">Check "Append" to add generated posts to existing ones.</p>
            <p id="sectionDescription">Uncheck "Append" to delete the existing posts & overwrite generated ones on
                top.</p>
        </footer>
    </article>
</div>