<?php
include($_SERVER['DOCUMENT_ROOT'] . "/blog/model/classes.php");
$post = new posts();
if (isset($_POST['action'])) {
    $post->fillEditingPostData($_POST['action']);
}
$record = $post->getOldPostData();
?>

<article class="topContent">
    <header>
        <h2><a href="#" title="<?php echo($record[6]); ?>"><?php echo($record[6]); ?></a></h2>
    </header>
    <footer>
        <p class="post-author">Author: <?php echo($record[1]); ?></p>
    </footer>
    <content>
        <?php echo($record[2]); ?>
    </content>
    <footer>
        <p class="post-date">Publish date: <?php echo($record[3]); ?></p>
        <p class="post-time">Publish time: <?php echo($record[5]); ?></p>
    </footer>
</article>